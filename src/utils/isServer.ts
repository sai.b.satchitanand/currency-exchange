export default function isServer(): boolean {
  return !(process && process.browser);
}
