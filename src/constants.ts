export const currenciesForExchange = ['USD', 'GBP', 'EUR'] as const;

export type ExchangeCurrency = typeof currenciesForExchange[number];

type CurrencySymbol = {
  [k in ExchangeCurrency]: string;
};

export const CurrencySymbol: CurrencySymbol = {
  EUR: '€',
  GBP: '£',
  USD: '$',
};
