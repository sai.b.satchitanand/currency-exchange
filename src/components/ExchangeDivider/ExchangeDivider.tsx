import React, { useContext } from 'react';
import SwapVertOutlinedIcon from '@material-ui/icons/SwapVertOutlined';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import styled from '@emotion/styled';
import { useObserver } from 'mobx-react-lite';
import { ExchangeStoreContext } from '../../stores';
import { useExchangeRates } from '../../hooks/useExchangeRate';
import { CurrencySymbol } from '../../constants';

const ExchangeDividerWrapper = styled.div`
  display: flex;
  padding: 0 16px;
  background: linear-gradient(to bottom, white 50%, #f3f4f5 50%);
`;

const SwapButton = styled.button`
  cursor: pointer;
  border-radius: 50%;
  border: 1px solid #8b959e;
  width: 30px;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ExchangePrice = styled.span`
  margin: 0 auto;
  display: flex;
  align-items: center;
  background-color: white;
  color: #0075eb;
  border: 1px solid #8b959e;
  font-size: 14px;
  border-radius: 18px;
  min-width: 120px;
  justify-content: center;
`;

export const ExchangeDivider: React.FC = () => {
  const exchangeStore = useContext(ExchangeStoreContext);

  return useObserver(() => {
    const { data } = useExchangeRates(exchangeStore.exchangeSet.FROM);

    return (
      <ExchangeDividerWrapper>
        <SwapButton
          onClick={(): void => {
            exchangeStore.swapCurrency();
          }}
        >
          <SwapVertOutlinedIcon style={{ color: '#0075eb', fontSize: '1rem' }}></SwapVertOutlinedIcon>
        </SwapButton>
        <ExchangePrice>
          <TrendingUpIcon style={{ fontSize: '1rem', paddingRight: '4px' }} />
          {data
            ? `${CurrencySymbol[exchangeStore.exchangeSet.FROM]}1 = ${
                CurrencySymbol[exchangeStore.exchangeSet.TO]
              }${data.rates[exchangeStore.exchangeSet.TO].toFixed(4)}
                `
            : ''}
        </ExchangePrice>
      </ExchangeDividerWrapper>
    );
  });
};
