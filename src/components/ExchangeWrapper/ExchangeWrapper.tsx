import styled from '@emotion/styled';

export const ExchangeWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  flex-direction: row;
`;

export const ExchangeContainer = styled.div`
  width: 100%;
  max-width: 400px;
  height: 100vh;
  border: 1px solid #f0f0f0;
  background-color: #f3f4f5;
`;

export const Header = styled.div`
  padding: 16px;
  padding-bottom: 0;
  font-size: 24px;
  font-weight: 500;
  background-color: #fff;
`;

export const InputNumber = styled.input`
  width: 100%;
  border: 0;
  font-size: 24px;
  text-align: right;
  background-color: transparent;
  &:focus {
    outline: 0;
  }
`;
