import React from 'react';

import { Header } from '../ExchangeWrapper';
import { ExchangePocket } from '../ExchangePocket';
import { ExchangeDivider } from '../ExchangeDivider';
import styled from '@emotion/styled';
import { ExchangeButton } from '../ExchangeButton';

const Container = styled.div`
  height: 100%;
  position: relative;
  .from {
    height: 80px;
    padding-top: 40px;
    background-color: #fff;
  }
  .to {
    padding-top: 40px;
  }
`;

export const ExchangeWidget: React.FC = () => {
  return (
    <Container>
      <Header>Exchange</Header>
      <ExchangePocket className="from" type="FROM"></ExchangePocket>
      <ExchangeDivider></ExchangeDivider>
      <ExchangePocket className="to" type="TO"></ExchangePocket>
      <ExchangeButton />
    </Container>
  );
};
