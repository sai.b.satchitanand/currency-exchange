import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';

import { ExchangePocket } from './ExchangePocket';
import { ExchangeStore, ExchangeStoreContext } from '../../stores';

jest.mock('../../hooks/useExchangeRate', () => ({
  useExchangeRates: jest.fn().mockReturnValue({
    data: 'hello',
  }),
}));

describe('ExchangePocket:', () => {
  let store: ExchangeStore;

  afterEach(cleanup);

  beforeEach(() => {
    store = new ExchangeStore();
  });

  test('should call store setExchangePrice on input field', () => {
    spyOn(store, 'setExchangePrice');

    const { getByTestId } = render(
      <ExchangeStoreContext.Provider value={store}>
        <ExchangePocket type="FROM"></ExchangePocket>
      </ExchangeStoreContext.Provider>,
    );
    const currencyInput = getByTestId('input-currency');
    fireEvent.change(currencyInput, { target: { value: '12' } });

    expect(store.setExchangePrice).toHaveBeenCalled();
  });
});
