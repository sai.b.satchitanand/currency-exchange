import React, { useContext } from 'react';
import Select from 'react-select';
import styled from '@emotion/styled';

import { ExchangeStoreContext } from '../../stores';
import { ExchangeType } from '../../types/exchange';
import { useObserver } from 'mobx-react-lite';
import { InputNumber } from '../ExchangeWrapper';
import { CurrencySymbol, ExchangeCurrency } from '../../constants';
import { useExchangeRates } from '../../hooks/useExchangeRate';

type BalanceProps = {
  warning: boolean;
};
type CurrencySelectOption = { label: ExchangeCurrency; value: ExchangeCurrency };

const Balance = styled.span<BalanceProps>`
  padding-top: 6px;
  color: ${(props): string => (props.warning ? '#eb008d' : '#969696')};
  font-size: 12px;
`;

const PocketWrapper = styled.div`
  padding: 16px;
  display: flex;
  align-items: baseline;
  .currency {
    width: 140px;
  }
`;

type ExchangePocket = {
  type: ExchangeType;
  className?: string;
};

export const ExchangePocket = ({ type, className }: ExchangePocket): JSX.Element => {
  const exchangeStore = useContext(ExchangeStoreContext);

  return useObserver(() => {
    const balance = exchangeStore.wallet[exchangeStore.exchangeSet[type]];
    const { data, error } = useExchangeRates(exchangeStore.exchangeSet[type]);

    if (error || !data) {
      return <></>;
    }

    return (
      <PocketWrapper className={className}>
        <div className="currency">
          <Select
            instanceId={type}
            value={{ label: exchangeStore.exchangeSet[type], value: exchangeStore.exchangeSet[type] }}
            options={exchangeStore.currenciesSet[type].map(curr => ({ label: curr, value: curr }))}
            onChange={(selectedCurrency): void => {
              exchangeStore.setExchangeSet(type, ((selectedCurrency as unknown) as CurrencySelectOption).value);
            }}
          />
          <Balance warning={type === 'FROM' && !exchangeStore.isExchangePossible}>
            Balance: {CurrencySymbol[exchangeStore.exchangeSet[type]]}
            {balance.toLocaleString('en-US', { maximumFractionDigits: 2 })}
          </Balance>
        </div>
        <InputNumber
          data-testid="input-currency"
          type="number"
          placeholder="0"
          min="0"
          max={`${balance}`}
          value={`${exchangeStore.exchangeAmount[type].formattedValue}`}
          onChange={({ target }): void => {
            exchangeStore.setExchangePrice(type, target.value, data);
          }}
        />
      </PocketWrapper>
    );
  });
};
