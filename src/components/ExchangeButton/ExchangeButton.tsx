import React, { useContext } from 'react';
import styled from '@emotion/styled';
import { useObserver } from 'mobx-react-lite';
import { ExchangeStoreContext } from '../../stores';

const Button = styled.button`
  position: absolute;
  bottom: 24px;
  left: 0;
  right: 0;
  width: calc(100% - 64px);
  height: 50px;
  border-radius: 28px;
  margin: 0 auto;
  display: block;
  background-color: #eb008d;
  color: white;
  font-size: 22px;
  cursor: pointer;

  &:disabled {
    background-color: #eb008d80;
    cursor: inherit;
  }
`;

export const ExchangeButton: React.FC = () => {
  const exchangeStore = useContext(ExchangeStoreContext);

  return useObserver(() => (
    <Button
      disabled={!exchangeStore.isExchangePossible}
      onClick={(): void => {
        exchangeStore.exchange();
      }}
    >
      Exchange
    </Button>
  ));
};
