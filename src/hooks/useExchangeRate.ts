import useSWR, { responseInterface } from 'swr';

import { ExchangeRate } from './../types/exchange';
import fetch from '../libs/fetch';
import { ExchangeCurrency } from '../constants';

export const useExchangeRates = (from: ExchangeCurrency): responseInterface<ExchangeRate, Error> => {
  return useSWR(`https://api.exchangerate-api.com/v4/latest/${from}`, fetch, {
    refreshInterval: 10000, // 10 secs
  });
};
