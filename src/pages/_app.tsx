import React from 'react';
import App from 'next/app';
import { Global, css } from '@emotion/core';

const globalCss = `
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance:textfield;
}
`;
class MyApp extends App {
  render(): JSX.Element {
    const { Component, pageProps } = this.props;

    return (
      <>
        <Global
          styles={css`
            ${require('normalize.css')}
            ${globalCss}
          `}
        />
        <Component {...pageProps} />
      </>
    );
  }
}

export default MyApp;
