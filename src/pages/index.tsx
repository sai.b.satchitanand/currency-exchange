import React from 'react';
import { ExchangeWrapper, ExchangeContainer } from '../components/ExchangeWrapper';
import { ExchangeWidget } from '../components/ExchangeWidget';
import { ExchangeStoreContext, ExchangeStore } from '../stores';

const Home: React.FC = () => {
  return (
    <ExchangeStoreContext.Provider value={ExchangeStore.getInstance()}>
      <ExchangeWrapper>
        <ExchangeContainer>
          <ExchangeWidget></ExchangeWidget>
        </ExchangeContainer>
      </ExchangeWrapper>
    </ExchangeStoreContext.Provider>
  );
};

export default Home;
