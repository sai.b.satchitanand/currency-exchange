import { ExchangeStore } from './ExchangeStore';
import { ExchangeRate } from '../types/exchange';

describe('ExchangeStore:', () => {
  const exchanges = ({
    rates: {
      USD: 1,
      GBP: 1.3,
      EUR: 0.78,
    },
  } as unknown) as ExchangeRate;
  let exchangeStore: ExchangeStore;

  beforeEach(() => {
    exchangeStore = new ExchangeStore();
  });

  describe('setExchangePrice', () => {
    test('should not change the formatted value to parse float when input in partially ending with point', () => {
      exchangeStore.setExchangePrice('FROM', '12', exchanges);
      exchangeStore.setExchangePrice('FROM', '12.', exchanges);

      expect(exchangeStore.exchangeAmount.FROM.formattedValue).toBe('12.');
      expect(exchangeStore.exchangeAmount.FROM.value).toBe(12);
    });

    test('should set price to `0` and format it blank string', () => {
      exchangeStore.setExchangePrice('FROM', '', exchanges);

      expect(exchangeStore.exchangeAmount.FROM.formattedValue).toBe('');
      expect(exchangeStore.exchangeAmount.FROM.value).toBe(0);
    });

    test('should round off to 2 decimal places', () => {
      exchangeStore.setExchangePrice('FROM', '12', exchanges);

      expect(exchangeStore.exchangeAmount.TO.formattedValue).toMatchInlineSnapshot(`"9.36"`);
      expect(exchangeStore.exchangeAmount.TO.value).toMatchInlineSnapshot(`9.36`);
    });
  });

  test('currenciesSet: from and to currency should not have same option', () => {
    expect(exchangeStore.currenciesSet.FROM.includes(exchangeStore.exchangeSet.TO)).toBeFalsy();
    expect(exchangeStore.currenciesSet.TO.includes(exchangeStore.exchangeSet.FROM)).toBeFalsy();
  });

  test('setExchangeSet: set the currency and reset the exchange price', () => {
    exchangeStore.setExchangeSet('FROM', 'USD');
    exchangeStore.setExchangePrice('FROM', '12', exchanges);
    exchangeStore.setExchangeSet('FROM', 'EUR');

    expect(exchangeStore.exchangeSet.FROM).toBe('EUR');
    expect(exchangeStore.exchangeAmount.FROM.value).toBe(0);
    expect(exchangeStore.exchangeAmount.TO.value).toBe(0);
  });

  test('exchange: after exchange the entered exchange amount should be reset', () => {
    exchangeStore.setExchangeSet('FROM', 'USD');
    exchangeStore.setExchangePrice('FROM', '12', exchanges);
    exchangeStore.exchange();

    expect(exchangeStore.exchangeAmount.FROM.value).toBe(0);
    expect(exchangeStore.exchangeAmount.TO.value).toBe(0);
  });

  test('swapCurrency: should swap exchange currency and amount', () => {
    exchangeStore.setExchangeSet('FROM', 'USD');
    exchangeStore.setExchangeSet('TO', 'EUR');
    exchangeStore.setExchangePrice('FROM', '12', exchanges);
    exchangeStore.swapCurrency();

    expect(exchangeStore.exchangeSet.FROM).toBe('EUR');
    expect(exchangeStore.exchangeSet.TO).toBe('USD');
    expect(exchangeStore.exchangeAmount.TO.value).toBe(12);
  });
});
