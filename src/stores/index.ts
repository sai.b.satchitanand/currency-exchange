import { configure } from 'mobx';
import { useStaticRendering } from 'mobx-react-lite';
import isServer from '../utils/isServer';

useStaticRendering(isServer());
configure({
  enforceActions: 'observed',
});

export * from './context';
export * from './ExchangeStore';
