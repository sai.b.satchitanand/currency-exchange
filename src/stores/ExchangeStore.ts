import { ExchangeRate } from './../types/exchange';
import { observable, action, computed } from 'mobx';

import isServer from '../utils/isServer';
import { currenciesForExchange, ExchangeCurrency } from '../constants';
import { ExchangeType } from '../types/exchange';

export type Wallet = {
  [k in ExchangeCurrency]: number;
};
export type ExchangeSet = {
  [k in ExchangeType]: ExchangeCurrency;
};
export type CurrencyExchangeSet = {
  [k in ExchangeType]: ExchangeCurrency[];
};
export type ExchangeAmount = {
  [k in ExchangeType]: { value: number; formattedValue: string };
};

const resetAmount = { value: 0, formattedValue: '' };
const round = (amount: number): number => Math.floor(amount * 100) / 100;
export class ExchangeStore {
  private static instance: ExchangeStore;

  @observable wallet: Wallet = {
    USD: 500,
    GBP: 300,
    EUR: 600,
  };
  @observable exchangeSet: ExchangeSet = {
    FROM: 'USD',
    TO: 'EUR',
  };
  @observable exchangeAmount: ExchangeAmount = {
    FROM: { ...resetAmount },
    TO: { ...resetAmount },
  };

  @action resetExchangePrice(): void {
    this.exchangeAmount.FROM = { ...resetAmount };
    this.exchangeAmount.TO = { ...resetAmount };
  }

  @action setExchangeSet(type: ExchangeType, value: ExchangeCurrency): void {
    this.exchangeSet[type] = value;
    this.resetExchangePrice();
  }

  @action swapCurrency(): void {
    [this.exchangeSet.FROM, this.exchangeSet.TO] = [this.exchangeSet.TO, this.exchangeSet.FROM];
    [this.exchangeAmount.FROM, this.exchangeAmount.TO] = [this.exchangeAmount.TO, this.exchangeAmount.FROM];
  }

  @action exchange(): void {
    this.wallet[this.exchangeSet.FROM] -= this.exchangeAmount.FROM.value;
    this.wallet[this.exchangeSet.TO] += this.exchangeAmount.TO.value;
    this.resetExchangePrice();
  }

  @computed
  get isExchangePossible(): boolean {
    if (this.exchangeAmount.FROM.value > this.wallet[this.exchangeSet.FROM] || this.exchangeAmount.FROM.value < 0) {
      return false;
    }

    return true;
  }

  @action setExchangePrice(type: ExchangeType, value: string, exchange: ExchangeRate): void {
    let price = parseFloat(value);

    if (isNaN(price)) {
      price = 0;
    }
    if (price && `${price}` !== value) {
      this.exchangeAmount[type].formattedValue = value;
      return;
    }
    const fromValue = price * exchange.rates[this.exchangeSet.FROM];
    const toValue = price * exchange.rates[this.exchangeSet.TO];

    this.exchangeAmount.FROM.value = fromValue;
    this.exchangeAmount.TO.value = toValue;
    this.exchangeAmount.FROM.formattedValue = fromValue ? `${round(fromValue)}` : '';
    this.exchangeAmount.TO.formattedValue = toValue ? `${round(toValue)}` : '';
  }

  @computed
  get currenciesSet(): CurrencyExchangeSet {
    return {
      FROM: currenciesForExchange.filter(curr => curr !== this.exchangeSet.TO),
      TO: currenciesForExchange.filter(curr => curr !== this.exchangeSet.FROM),
    };
  }

  static getInstance(): ExchangeStore {
    if (isServer() || this.instance === undefined) {
      return (this.instance = new this());
    }
    return this.instance;
  }
}
