import React from 'react';
import { ExchangeStore } from './ExchangeStore';

export const ExchangeStoreContext = React.createContext<ExchangeStore>(ExchangeStore.getInstance());
