# Currency-Exchange

## Demo
[https://currency-exchange.saibs.now.sh](https://currency-exchange.saibs.now.sh)

Pre-requisite:
- nodejs

Installation instructions:

```sh
git clone git@gitlab.com:sai.b.satchitanand/currency-exchange.git
yarn
```

For development
```sh
yarn dev
```

or to run application in production
```sh
yarn build
yarn start
```

Primary technologies used:
- [React](https://reactjs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [eslint](https://eslint.org/blog/2019/01/future-typescript-eslint)
- [Nextjs](http://nextjs.org) - SSR framework for react, out of the box routing and easy customization.
- [mobx](https://mobx.js.org/) - State management.
- [Jest](https://jestjs.io/)
- [react-testing-library](https://testing-library.com/docs/react-testing-library/intro) For component testing.
- [commitzen](http://commitizen.github.io/cz-cli/) Standardizing commit messages.
